# ppp.css — PrePostPrint only css framework

----
**FR** 

### Installation
- Ajouter le fichier ppp.css dans le dossier de votre site
- Lier le fichier ppp.css dans le head de votre html ``<link rel="stylesheet" href="ppp.css">``
- Si vous souhaitez prévisualiser l'impression dans le navigateur, ajouter le ficher pppreview.css
- lier le ficher pppreview.css dans le head de votre html `` <link rel="stylesheet" href="pppreview.css"> ``

### Usage

#### Navigateur

Pour visualiser et imprimer votre publication utiliser Chrome ou Chromium comme navigateur.

#### Éditer les variables de format de la publication
Au début du fichier ppp.css, éditer les variables.
<pre><code>:root {
	/* Largeur de la page / Page width */
	--page-width: 14.85cm;
	/* Hauteur de la page / Page height */
	--page-height: 21cm;
	/* Marge de la page / Page margins */
	--page-margin: 0.8cm;
	/* position du numéro de page / Page counter position */
	--page-footer-bottom: 0.4cm;
	--page-footer-margin: 0.8cm;
}</code>
</pre>


#### Structure HTML
Chaque page se structure ainsi <br>
``<section class="page"><!--  html content --></section>``


#### Ajouter un titre courant

Ajouter la classe ``.introheader`` aux sections portant les classes ``.page`` <br>
- *exemple* : ``<section class="page introheader"></section>``

Utiliser ``::before`` en css, et remplacer content: par le texte désiré<br>
- *exemple* :  ``.introheader::before{ content:"titre courant";}``

#### Utiliser le framework
Dans le HTML :
- ``.print``: les éléments avec cette classe s'affiche seulement à l'impression
- ``.screen``: les éléments avec cette classe s'affiche seulement à l'écran
- ``.center``: centre l'élément horizontalement
- ``.center-top``: centre l'élément horizontalement
- ``.nopagination``: enlève la pagination des pages (.page) ayant cette classe
- ``.plainimage``: ajouter cette classe à un élément img pour adapter l'image à la page


----

**EN**

### Installation
- Add ppp.css to your website folder
- Link ppp.css file in your HTML head : ``<link rel="stylesheet" href="ppp.css">``
- If you want to have a print preview in the browser, add the pppreview. css in your website folder
- link the file pppreview.css in your HTML head : ``<link rel="stylesheet" href="pppreview.css">``

### Use

#### Browser preview

To display and print your publication use Chrome or Chromium as browser.

#### Edit the publication format variables

At the top of ppp.css, edit the variables.
<pre><code>:root {
	/* Largeur de la page / Page width */
	--page-width: 14.85cm;
	/* Hauteur de la page / Page height */
	--page-height: 21cm;
	/* Marge de la page / Page margins */
	--page-margin: 0.8cm;
	/* position du numéro de page / Page counter position */
	--page-footer-bottom: 0.4cm;
	--page-footer-margin: 0.8cm;
}</code>
</pre>

#### Html structure

Each page are structure by section and class : <br>
``<section class="page"><!--  html content --></section>``

#### Running-title

To add runing-title to the page header add  ``.introheader`` class to the ``.page``
- *example* : ``<section class="page introheader"></section>``

To edit the runing-title content, use ``::before`` and replace the content rules
- *example* : ``.introheader::before{ content:"titre courant";}``

#### Use the framework

In the HTML :
 - ``.print``: elements with this class are displayed only when printed
- ``.screen``: elements with this class are displayed only on the screen
- ``.center``: centers the element horizontally
- ``.center-top``: center the element horizontally
- ``.nopagination``: remove pagination from pages (.page) with this class
- ``.plainimage``: add this class to an img element to fit the image to the page
